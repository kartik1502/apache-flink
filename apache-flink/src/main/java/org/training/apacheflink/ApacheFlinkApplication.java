package org.training.apacheflink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApacheFlinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApacheFlinkApplication.class, args);
	}

}
