package org.training.apacheflink.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Captializer {

    public static void main(String[] args) throws IOException {
        
        DataStream < String > dataStream = null;
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        final ParameterTool params = ParameterTool.fromArgs(args);
        env.getConfig().setGlobalJobParameters(params);
        if (params.has("input") && params.has("output")) {
         //data source
            byte[] fileContent = Files.readAllBytes(Path.of(params.get("input")));
            dataStream = env.fromData(fileContent.toString());
        } else {
            System.err.println("No input specified. Please run 'UpperCaseTransformationApp --input <file-to-path> --output <file-to-path>'");
            return;
        }
        if (dataStream == null) {
            System.err.println("DataStream created as null, check file path");
            System.exit(1);
            return;
        }
  //transformation
        dataStream = dataStream.map(String::toUpperCase);
        byte[] output = dataStream.toString().getBytes();

        Files.write(Path.of(params.get("output")), output);
        try {
            env.execute("read and write");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
